import numpy as np
import cv2
import time

def compare_list(lis: list):
    for i in lis:
        if not i:
            return False
    return True

cap = cv2.VideoCapture('video2.mp4')
while(cap.isOpened()):
    ret, frame = cap.read()
    show = frame.copy()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 130, 203, 203)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(show, contours, -1, (0, 0, 255), 1)
    # cv2.drawContours(frame, [contours[0]], 0, (0, 0, 255), 2)
    if hierarchy.shape[1] in range(14, 16):
        x = int(hierarchy[0, 6, 3])
        if compare_list(hierarchy[0, 7:11, 3] == x):
            if cv2.arcLength(contours[1], False) > 446.0 and cv2.arcLength(contours[1], False) < 449.0:
                print("МОЯ КАРТИНКА!")
                cv2.imwrite("mypic.png", frame)

    cv2.imshow('frame',show)
    #print(hierarchy.shape)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    
cap.release()
cv2.destroyAllWindows()
